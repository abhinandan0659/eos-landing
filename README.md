# EOS project landing page
![Open Source Love png2](https://badges.frapsoft.com/os/v2/open-source.png?v=103)
![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)

Demo: https://www.eosdesignsystem.com/

### Installation

1. `git clone git@gitlab.com:SUSE-UIUX/eos-landing.git`
2. `cd eos-landing`
3. Make sure you have the corresponding version of Node and NPM:  
  a.
    ```
      sudo npm cache clean -f
      sudo npm install -g n
      sudo n 10.15.0
    ```
  b.
    ```
      sudo npm install -g npm@6.9.0
    ```   
4. `npm install --engine-strict`  
5. `npm start`  
6. It will build your assets & open: `dist/index.html` in your browser
7. It will also start the `gulp watch` task, which will watch changes in the assets

### Running lints:

Test all:
`npm run test:all`

Sass:
`npm run test:sass`

JS:
`npm run test:js`

Pug:
`npm run test:pug`

Unit testing:
`npm run test:unit`

Generate vendors folder:
`npm run build:vendors`


# Our "thank you" section

### Tested for every browser in every device

Thanks to [browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.


### Projects using EOS

- [openSUSE](https://www.opensuse.org/)
- [HAWK](https://hawk-ui.github.io/)
